package fr.siovhb.velostar;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class StationVeloTest {
    @Test
    public void creerStationTest() {
        StationVelo uneStation;
        uneStation = new StationVelo(1, "Villejean", 15, 20);
        assertNotNull(uneStation);
        assertEquals("Villejean", uneStation.getNom());
    }
    @Test
    public void getNbVelosDisponiblesTest() {
        StationVelo uneStation;
        uneStation = new StationVelo(2, "John Kennedy", 20, 25);
        assertNotNull(uneStation);
        assertEquals(25, uneStation.getNbVelosDisponibles());
    }
    @Test
    public void getNbAttachesDisponiblesTest() {
        StationVelo uneStation;
        uneStation = new StationVelo(2, "John Kennedy", 20, 25);
        assertNotNull(uneStation);
        assertEquals(20, uneStation.getNbAttachesDisponibles());
    }
}
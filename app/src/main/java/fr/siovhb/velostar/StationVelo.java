package fr.siovhb.velostar;
/**
 * Cette classe décrit une station de VéloStar
 *
 */
public class StationVelo {
	/** Membres privés */
	private int _id;					// id de la station
	private String _nom;				// nom de la station
	private int _nbAttachesDisponibles;	// nombre d'attaches disponibles
	private int _nbVelosDisponibles;	// nombre de vélos disponibles

	/**
	 * Initialise une instance de station
	 * @param id
	 * @param nom
	 * @param nbAttachesDisponibles
	 * @param nbVelosDisponibles
	 */
	public StationVelo(int id, String nom, int nbAttachesDisponibles, int nbVelosDisponibles)
	{	this._id = id;
		this._nom = nom.trim();
		this._nbAttachesDisponibles = nbAttachesDisponibles;
		this._nbVelosDisponibles = nbVelosDisponibles;
	}
	/** Accesseurs */
	public int getId()
	{	return this._id;
	}
	public String getNom() {
		return _nom;
	}
	public int getNbAttachesDisponibles() {
		return _nbAttachesDisponibles;
	}
	public int getNbVelosDisponibles() {
		return _nbVelosDisponibles;
	}
}
